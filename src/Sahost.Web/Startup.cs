﻿using B2Net;
using HashidsNet;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Sahost.Data;
using Sahost.Web.Config;

namespace Sahost.Web
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }

        public Startup()
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false, true);

            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("Main");
            services.AddEntityFrameworkNpgsql()
                .AddDbContext<ApiContext>(options => options.UseNpgsql(connectionString, o => o.UseNodaTime()));

            services.AddMvc();

            var backblazeConfigSection = Configuration.GetSection("Backblaze");
            var backblazeOptions = backblazeConfigSection.Get<BackblazeConfig>();
            var backblazeClient = new B2Client(B2Client.Authorize(backblazeOptions.AccountId, backblazeOptions.ApplicationKey));

            var hashids = new Hashids(Configuration["HashidsSalt"], 5);

            services.Configure<BackblazeConfig>(backblazeConfigSection);
            services.AddSingleton(backblazeClient);
            services.AddSingleton(hashids);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
