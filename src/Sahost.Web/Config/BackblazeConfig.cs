﻿namespace Sahost.Web.Config
{
    public class BackblazeConfig
    {
        public string AccountId { get; set; }

        public string BucketId { get; set; }

        public string ApplicationKey { get; set; }
    }
}
