﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Sahost.Web
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseKestrel(o => o.AddServerHeader = false)
                .UseStartup<Startup>();
    }
}
