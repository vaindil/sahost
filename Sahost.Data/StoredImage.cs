using NodaTime;
using System.Net;

namespace Sahost.Data
{
    public class StoredImage
    {
        public long Id { get; set; }

        public string Filename { get; set; }

        public Instant UploadedAt { get; set; }

        public IPAddress IpAddress { get; set; }
    }
}
