using Microsoft.EntityFrameworkCore;

namespace Sahost.Data
{
    public class ApiContext : DbContext
    {
        public ApiContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<StoredImage> StoredImages { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<StoredImage>(e =>
            {
                e.ToTable("stored_image");
                e.HasKey(x => x.Id);

                e.Property(x => x.Id).HasColumnName("id");
                e.Property(x => x.Filename).HasColumnName("filename").HasColumnType("text");
                e.Property(x => x.UploadedAt).HasColumnName("uploaded_at");
                e.Property(x => x.IpAddress).HasColumnName("ip_address");
            });
        }
    }
}
